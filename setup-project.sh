#this script is only meant to be used with the virutal machine that I have created this for; other users can use at their own risk. 

#AUSER = $1
#REPOSITORY = $2
#PASSWORD = $3
#FOLDER = $4
#Djang0 settings module = $5

#Example for most modern version of django
#sh setup-project.sh sg sg.thinkfirstmarketing.com//home/sg/ 'SG production ftw!' sg_dev sg sg.thinkfirstmarketing.com:/home/sg/sg_dev/sg/media

#virtual machine URL that this is meant to work with  https://www.dropbox.com/s/g9fujltxay1y924/Ubuntu.ova

#go to the websites folder
#checkout the code
cd /home/dev/websites
echo running: git clone ssh://$1@$2 $4
git clone ssh://$1@$2/$1.git $4

echo cd $4
cd $4

#create, activate, and initialize the virtualenvironment 
virtualenv env
. env/bin/activate
pip install -r requirements.txt

#if the last two parameters exist, add them 
if [ -n '$5' ] 
then
    export DJANGO_SETTINGS_MODULE=$5.settings.local
fi 

if [ -n '$6' ] 
then
    scp -r $5@$6 $5/media
fi

#sync and migrate database
python manage.py syncdb
python manage.py migrate --merge

#run the server
python manage.py runserver &

#open chrome to the URL 
chromium-browser http://127.0.0.1:8000 &